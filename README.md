# slim skeleton

a slim framework skeleton.

## run project on local

> required install `php` at local, and add to `PATH`.

### at windows (usage php server)

- auto run server
> double-click `bin/loclaRun.bat` file.

- manual run server
> open a cmd window at current directory, write command: `php -S 127.0.0.42:80 -t public`, open a browser (e.g. chrome) visit `http://127.0.0.42`

### at linux (usage php server)

- auto run server
> `$ ./bin/loclaRun`

- manual run server
> open a terminal window jump to current directory, run command: `php -S 127.0.0.42:80 -t www`, open a browser (e.g. chrome) visit `http://127.0.0.42`

## run with nginx service

> please copy the file `temp/backup/example.nginx.conf` to nginx config directory, and rename it. reload nginx service.