@echo on

@setlocal

set IP_PORT=127.0.0.41:80
set WEB_DIR=../public

start http://"%IP_PORT%"

set BASE_PATH=%~dp0

if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe

echo "> php -S %IP_PORT% -t public"
echo "# Please open browser visit http://%IP_PORT%"
echo "# Current directory is: %BASE_PATH%"
echo.

"%PHP_COMMAND%" -S "%IP_PORT%" -t "%WEB_DIR%"

@endlocal
