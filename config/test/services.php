<?php
/**
 * in the test env.
 * services config
 * @var $config \slimExt\DataCollector
 */

$settings = require dirname(__DIR__) . '/services.php';

// override default config
// $settings['db'] = [
//     ...
// ];
// add new service config
// $settings['custom'] = [
//     ...
// ];

return array_merge($settings, (array)$config->remove('services'));