<?php
/**
 * this is service settings.
 * @var $config \slimExt\DataCollector
 */

return [
    'displayErrorDetails' => true, // set to false in production
    // 'charset'       => $config->get('charset','UTF-8'),

    'db' => [
        'dsn' => '',
    ],

    'cache.files' => [
        'path' => '@temp/caches',
        'compress_data' => true,
    ],

    'cache.sqlite' => [
        'path' => '@temp/caches',
    ],

    'cache.redis' => [
            'host' => 'redis',
            'port' => 6379,
            'database' => 10,

            'options' => [
//                    'prefix' => 'blog:'
            ]
    ],
    'redis' => [
        'parameters' => [
            'tcp://redis:6379?database=0&alias=master',
            'tcp://redis:6380?database=0&alias=slave',
        ],
        'options' => [
            'replication' => true
        ],
    ],

    'user' => [
        'identityClass' => \app\models\Users::class,
    ],

    // language
    'language'       => [
        'lang'     => $config->get('language', 'en'),
        'basePath' => '@resources/languages',

        'langFiles' => [
            // key => file path
            // if no file key, default use file name. e.g: app.yml -> app
            'app.yml',
            'contents.yml',
            'user.yml',
        ],
    ],

    // Whoops tool config
    'debug'         => $config->get('debug', false),
    'whoops.editor' => 'sublime', // Support click to open editor
    'whoops.errLog' => 'full', // 'simple' or 'full'

    // Renderer settings
    'renderer' => [
        'template_path' => PROJECT_PATH . '/resources/views/',
        'tpl_suffix' => 'php',

        // add tpl global var key.
        'global_var_key' => '_globals',
        // add tpl global var. usage at view `$_globals['name'] `. if use twig `{{ _globals.name }}`
        'global_var_list' => [
//                'name'    => 'value',
        ]
    ],

    // Twig Renderer settings
    'twigRenderer' => [
        'tpl_path'   => PROJECT_PATH . '/resources/views/',
        'tpl_suffix' => 'twig',
        'lexer_options' => [
//                'tag_variable' => ['{[', ']}'], // default is '{{', '}}' 这里为了避免与 vue.js 的 标签冲突
        ],
        'config'     => [
            'debug'      => $config->get('debug', false),
            'cache'      => $config->get('twigCache', true) ? PROJECT_PATH . '/temp/twig' : false,
            'strict_variables' => true
        ],
        // add tpl global var key.
        'global_var_key' => '_globals',
        // add tpl global var. usage {{ _globals.name }}
        'global_var_list' => [
//                'name'    => 'value',
           'srcAsset'    => '/assets/src/',
           'lteAsset'    => '/assets/libs/admin-lte/',
        ]
    ],

    // Monolog settings
    'logger' => [
        'name' => 'app-run',
        'path' => PROJECT_PATH . '/temp/logs/access/access-'.date('Ymd').'.log',
    ],

    // Monolog settings
    'dbLogger' => [
        'name' => 'app-db',
        'path' => PROJECT_PATH . '/temp/logs/database/db-'.date('Ymd').'.log',
    ],

    // Monolog settings
    'eventLogger' => [
        'name' => 'app-events',
        'path' => PROJECT_PATH . '/temp/logs/events/event-'.date('Ymd').'.log',
    ],

    // Monolog settings
    'errLogger' => [
        'name' => 'app-err',
        'path' => PROJECT_PATH . '/temp/logs/errors/err-'.date('Ymd').'.log',
    ],
];
