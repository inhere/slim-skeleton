<?php
/**
 * in the console.
 * services config
 * @var $config \slimExt\DataCollector
 */


$settings = require dirname(__DIR__) . '/services.php';

return array_merge($settings,[

    // language
    'language'       => [
        'lang'     => $config->get('language', 'en'),
        'basePath' => '@resources/languages',
    ],

    // Monolog settings
    'logger' => [
        'name' => 'app-run',
        'path' => PROJECT_PATH . '/temp/logs/access/access-'.date('Ymd').'.log',
    ],

], (array)$config->remove('services'));
