<?php
/**
 * @var $app        \Slim\App
 * @var $container  \slimExt\base\Container
 * @var $config     \slimExt\DataCollector
 */

// start session
session_start();

// load yaml config
$localFile  = PROJECT_PATH . '/config/local.yml';
$configFile = PROJECT_PATH . '/config/app.yml';
$config = \slimExt\DataCollector::make($configFile, \slimExt\DataCollector::FORMAT_YML, 'application')
        ->loadYaml(is_file($localFile) ? $localFile : '');// the local config

// Some setting
date_default_timezone_set($config->get('timeZone'));

define('RUNTIME_ENV', $config->get('env', PDT_RUNTIME_ENV) );

// Instantiate the container
$settings  = require dirname(__DIR__) . '/config/'.RUNTIME_ENV.'/services.php';
$container = new \slimExt\base\Container($settings);
$container['config'] = $config;

// Instantiate the app
//$app = new \Slim\App($settings);
Slim::$app = $app = new \slimExt\base\App($container);

// Register services
require __DIR__ . '/services.php';

// Register middleware
require __DIR__ . '/middleware.php';

// Register routes
require __DIR__ . '/routes.php';

// Run app
try {
    $app->run();
} catch (\Exception $e) {
    /** @var callable $handler */
    $handler = $container->raw('runExceptionHandler');
    $handler($container, $app);
}
