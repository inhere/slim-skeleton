<?php

// ANSI UTF8
// header('Content-Type: text/html; Charset = UTF8');

// check env
if ( PHP_SAPI !== 'cli' ) {
    header('HTTP/1.1 403 Forbidden');
    exit("  403 Forbidden \n\n"
        . " current environment is CLI. \n"
        . " :( Sorry! Run this script is only allowed in the terminal environment!\n,You are not allowed to access this file.\n");
}

// fcgi doesn't have STDIN and STDOUT defined by default
defined('STDIN')     or define('STDIN', fopen('php://stdin', 'r'));
defined('STDOUT')    or define('STDOUT', fopen('php://stdout', 'w'));
error_reporting(E_ALL);

$localFile  = PROJECT_PATH . '/config/local.yml'; // the local config
$file   = PROJECT_PATH . '/config/console/app.yml';
$config = \slimExt\DataCollector::make($file,\slimExt\DataCollector::FORMAT_YML,'console')
        ->loadYaml(is_file($localFile) ? $localFile : '');

// Some setting
date_default_timezone_set($config->get('timeZone'));
define('RUNTIME_ENV', $config->get('env', PDT_RUNTIME_ENV) );

$settings  = require PROJECT_PATH . '/config/console/services.php';

Slim::$app = $app = new \slimExt\base\ConsoleApp($settings);

if ($config->get('debug', false)) {
    $app->setCatchExceptions(false);
}

$container = $app->getContainer();
$container['config'] = $config;

// Register services
require __DIR__ . '/services.php';

// Register middleware
require __DIR__ . '/middleware.php';

// register command
require __DIR__ . '/commands.php';

$app->run();
