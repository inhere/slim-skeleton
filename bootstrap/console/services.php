<?php
/**
 *  DIC configuration
 *
 * @var $app        \Slim\App
 * @var $config     \slimExt\DataCollector
 * @var $container  \slimExt\base\Container
 * @return mixed
 */

//

// project language config
$container['language'] = function ($c) {
    /** @var $c slimExt\base\Container */
    $options = $c->get('settings')['language'];

    return new \slimExt\base\Language($options);
};

// monolog - request logger
$container['logger'] = function ($c) {
    /** @var $c slimExt\base\Container */
    $settings = $c->get('settings')['logger'];
    $logger = new \Monolog\Logger($settings['name']);
    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

