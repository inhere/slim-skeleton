<?php
/**
 * Routes
 * @var $app \slimExt\base\App
 */

use slimExt\middlewares\AuthCheck;
use app\controllers;
use app\controllers\api as api;

// set class and action
$app->get('/', 'app\controllers\Home:index');
$app->any('/test[/{action}]', controllers\Test::class);

// provide url rewrite
$app->get('/goto', controllers\Gooto::class . ':index');

// Route groups
$app->group('/api', function () {
    // $this->any('/users[/{action:\w+}]', api\User::class)->add(AuthCheck::class);
    $this->any('/test[/{argument}]', api\Test::class);
});

