<?php

namespace app\controllers;

use Slim;

/**
 * Class Home
 * @package app
 */
class Home extends Base
{
    public function indexAction($args)
    {
        return $this->render('index');
    }
}
