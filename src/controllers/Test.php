<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/3/8
 * Time: 17:48
 */

namespace app\controllers;

use inhere\librarys\files\Picture;
use inhere\validate\Validation;
use Slim;

/**
 * Class Test
 * @package app\controllers
 */
class Test extends Base
{
    public function indexAction($args)
    {
        de($args, $_SERVER);
    }

    public function showAction()
    {
        $sourceImg = 'assets/images/background/laminat.jpg';
        $img = $this->request->get('img', $sourceImg);

        Picture::show(PROJECT_PATH . '/public/' .$img);

        return $this->response->withoutHeader('Content-type');
    }

    public function validateAction($args)
    {
        $v = Validation::make($this->request->all(), [
            ['tagId,userId,freeTime', 'required', 'msg' => '{attr} is required!'],
            ['tagId', 'size', 'min'=>4, 'max'=>567, 'msg' => '{attr} is must in {min}~{max}!'], // 4<= tagId <=567
            ['name', 'size', 'min'=>2, 'max'=>567, 'msg' => '{attr} is must in {min}~{max}!'], // 4<= tagId <=567
            ['userId', 'int'], // 4<= tagId <=567
            ['userId', function($value){
                return true;
            }, 'msg' => '{attr} is not passed!'],
        ]);

        return $this->response->withJson($v->getErrors());
    }
}
