<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/12/20
 * Time: 下午9:15
 */

namespace app\controllers\api;

/**
 * Class Test
 * @package app\controllers\api
 */
class Test extends Base
{
    /**
     * @return array
     */
    protected function methodMapping()
    {
        $map = parent::methodMapping();

        $map['get|post.search'] = 'search';

        return $map;
    }

    public function getsAction()
    {

        return $this->response->withJson([
            'results' => [
                [
                    'name1' => 'value1'
                ],
                [
                    'name2' => 'value2'
                ]
            ]
        ]);
    }

    public function searchAction()
    {
        return $this->response->withJson([
            'name' => 'value'
        ]);
    }
}
