<?php
/**
 * Created by PhpStorm.
 * User: Inhere
 * Date: 2016/4/24 0024
 * Time: 19:27
 */

namespace app\controllers\api;

use Slim;
use slimExt\base\Request;
use slimExt\base\Response;
use inhere\tools\files\Captcha;

/**
 * Class Util
 * @package app\controllers\api
 */
class Util extends Base
{
    /**
     */
    public function captchaAction()
    {
        Captcha::make(Slim::config('captcha'))->show();

        return $this->response->withoutHeader('Content-type');
    }
}
